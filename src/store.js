import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	'title': 'Example app vuex',
  	links: [
  		'google.com',
  		'facebook.com'
  	]
  },
  getters: {
  	countLinks: state => state.links.length
  },
  mutations: {
  	ADD_LINK: (state, link) => state.links.push(link),
  	REMOVE_LINK: (state, link) => state.links.splice(link, 1),
  	REMOVE_ALL: (state) => state.links = []
  },
  actions: {
  	RemoveLink: (context, link) => context.commit('REMOVE_LINK', link),
  	RemoveAll: ({commit}) => {
  		return new Promise((resolve, reject) => {
  			setTimeout(()=> {
  				commit('REMOVE_ALL');
  				resolve()
  			}, 1500)
  		})
  	}
  }
})
